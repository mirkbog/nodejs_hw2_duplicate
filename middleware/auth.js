const {User} = require('../models');

const jwt = require('jsonwebtoken');

// eslint-disable-next-line no-undef
const {SECRET_KEY} = process.env;

const auth = async (req, res, next) => {

  const { authorization } = req.headers;
  const [bearer, token] = authorization.split(" ");

if(bearer === "Bearer" && token){
  try {

          const { id } = jwt.verify(token, SECRET_KEY);
 
          const user = await User.findById(id)

          if(user){
            req.user = user;
            next();
          }else{
            return res.status(400).json({
              message:"Unauthorized"
            })
          }
      
        } catch (err) {
          return res.status(400).json({
            message:"Unauthorized"
          })
        }
}
 
  }




module.exports = auth 