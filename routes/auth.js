const express = require('express');
const {auth:ctrl} = require('../controllers');
const wrapper = require('../middleware/wrapper')


const router = express.Router();

router.post('/register',  wrapper(ctrl.register));
router.post('/login', wrapper(ctrl.login))

module.exports = router;