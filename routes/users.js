const express = require('express');

const {users:ctrl} = require("../controllers");
const wrapper = require('../middleware/wrapper')

const router = express.Router();

router.get('/', wrapper(ctrl.getCurrent));

router.delete('/', wrapper(ctrl.deleteUser));

router.patch('/', wrapper(ctrl.updPassword))

module.exports = router;