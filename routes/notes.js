const express = require('express');


const {notes:ctrl} = require('../controllers');
const wrapper = require('../middleware/wrapper')

const router = express.Router();

router.get('/', wrapper(ctrl.getAll));
router.get('/:id',wrapper(ctrl.getById))

router.post('/',wrapper(ctrl.add));
router.delete('/:id',wrapper(ctrl.remove));

router.put('/:id',wrapper(ctrl.updateText));
router.patch('/:id',wrapper(ctrl.checkToggle))

module.exports = router;