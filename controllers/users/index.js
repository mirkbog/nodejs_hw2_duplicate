const getCurrent = require('./getCurrent');
const deleteUser = require('./deleteUser');
const updPassword = require('./updPassword');


module.exports = {
    getCurrent,
    deleteUser,
    updPassword
}