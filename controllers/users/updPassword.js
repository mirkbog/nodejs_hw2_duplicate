const bcrypt = require("bcryptjs");
const { User } = require("../../models");

const updPassword = async (req, res) => {
  const { _id } = req.user;
  const { oldPassword, newPassword } = req.body;

  const hashPassword = bcrypt.hashSync(newPassword, bcrypt.genSaltSync(10));
  const result = await User.findByIdAndUpdate(_id, { password: hashPassword });
  if (!result) {
   return  res.status(400).json({
      message: "Error",
    });
  }
  return res.status(200).json({
    message: "Success",
  });
};

module.exports = updPassword;
