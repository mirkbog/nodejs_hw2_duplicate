const {User} = require('../../models');

const getCurrent = async(req,res)=>{
    const {username,_id, createdDate}= req.user;
    return res.json({
        status:"success",
        code:200,
        user:{
            _id,
            username,
            createdDate
        }
     })
}

module.exports = getCurrent;