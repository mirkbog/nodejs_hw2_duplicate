const {User} = require('../../models');

const deleteUser = async(req,res)=>{
    const {_id} = req.user;
    const result = await User.findByIdAndRemove(_id);
    console.log(result)
    if(!result){
      return  res.status(400).json({
            message:"Error"
        })
    }
    return res.status(200).json({
        message:"Success"
     })
}

module.exports = deleteUser;