

const bcrypt = require('bcryptjs')
const {User} = require('../../models')

const register = async(req,res)=>{
const {username,password} = req.body;
const user = await User.findOne({username});
if(user){

   return res.status(400).json({
        message:`User with username ${username} already exists`
    })
}
const birthDate = new Date();
const hashPassword = await bcrypt.hash(password, bcrypt.genSaltSync(10))
await User.create({username,password:hashPassword, createdDate:birthDate});
return res.status(200).json({
    message:"Success"

})
}

module.exports = register;