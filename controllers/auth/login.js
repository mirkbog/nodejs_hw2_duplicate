const dotenv = require('dotenv');
dotenv.config();

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const {User} = require('../../models');
// eslint-disable-next-line no-undef
const {SECRET_KEY} = process.env;

const login = async(req,res)=>{

const {username,password} = req.body;

const user = await User.findOne({username});

    if(user && await bcrypt.compare(password, user.password)){
        const payload = {
            id: user._id
        }
        
        const token = jwt.sign(payload, SECRET_KEY);
        
        
        return res.status(200).json({
            message:'Success',
            jwt_token: token
        })
    }else{
        return  res.status(400).json({
            message:"User not found"
        })
    }




}

module.exports = login;