const {Product} = require('../../models');

const getById = async(req,res)=>{
    const {id} = req.params;
    const result = await Product.findById(id);
    if(!result){
     return  res.status(400).json({
        message:'Error'
       })
    }
   return res.json({
        status:'success',
        code:200,
        data:{
            result
        }
    })
}

module.exports = getById
