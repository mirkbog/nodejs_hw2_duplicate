const {Note} = require('../../models')

const getAll = async(req,res)=>{
    const {offset=0, limit=0} = req.query;

    const {_id} = req.user;
const notes = await Note.find(({userId:_id}, "", {offset,limit:Number(limit)}));
const count = await Note.find({userId:_id}).length;

if(!notes){
return    res.status(400).json({
        message:"Error"
    })
}

return res.status(200).json({
    offset,
    limit,
    count,
    notes
})
}

module.exports = getAll;