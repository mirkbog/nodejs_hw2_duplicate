const {Note} = require('../../models');

const checkToggle = async(req,res)=>{
    const {id} = req.params;
  const prevVal = await Note.findById(id);
    const result = await Note.findByIdAndUpdate(id, {completed:!prevVal.completed});
  
    if(!result){
      return  res.status(400).json({
            message:"Wasn't able to update your note"
        })
    }
   return res.status(200).json({
        message:"Success"
    })
}

module.exports = checkToggle;