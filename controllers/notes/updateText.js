const {Note} = require('../../models');

const updateText = async(req,res)=>{
    const {id} = req.params;
    const {text} = req.body;
    const result = await Note.findByIdAndUpdate(id, {text});
    if(!result){
     return   res.status(400).json({
            message:"Wasn't able to update your note"
        })
    }
    return res.status(200).json({
        message:"Success"
    })
}

module.exports = updateText;