const {Note} = require('../../models');


const remove=async(req,res)=>{
const {id} = req.params;
const result = await Note.findByIdAndRemove(id);
if(!result){
  return  res.status(400).json({
        message:`Did't find notes with id ${id}`
    })
}
return res.json({
    message:"Success"
})
}

module.exports = remove;