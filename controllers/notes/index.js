const getAll = require('./getAll');
const add = require('./add');
const getById = require('./getById');
const remove = require('./remove');
const updateText = require('./updateText');
const checkToggle = require('./checkToggle')

module.exports = {
    getAll,
    add,
    getById,
    remove,
    updateText,
    checkToggle
}