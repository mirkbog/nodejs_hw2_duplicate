const {Schema, model, mongoose} = require('mongoose');

const noteSchema = Schema({
    userId:{
        type:mongoose.Schema.Types.ObjectId,
        ref:'user',
        required:true
    },
    completed:{
        type:Boolean,
        required:true
    },
    text:{
        type:String,
        required:true
    },
    createdDate:{
        type:String,
        required:true
    }

})

const Note = model('note', noteSchema);

module.exports = Note;


