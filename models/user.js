const {Schema, model} = require('mongoose');

const userSchema = Schema({
    username:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    createdDate:{
        type:String,
        required:true
    }
})

const User = model('user', userSchema);

module.exports = User;