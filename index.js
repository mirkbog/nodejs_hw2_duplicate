//   mongodb+srv://elenitsa:stinamerikithapao@todo-app.hkxypvu.mongodb.net/test


const mongoose = require("mongoose");
const express = require('express');
const cors = require('cors');
require('dotenv').config();
let morgan = require('morgan')

const authMiddleware = require('./middleware/auth');


const {authRouter,usersRouter,notesRouter} = require('./routes')



// eslint-disable-next-line no-undef
const {DB_HOST, PORT} = process.env;

const app = express();

 
// setup the logger
app.use(morgan('combined'))


app.use(cors());
app.use(express.json());

app.use('/api/auth', authRouter);
app.use('/api/users/me', authMiddleware, usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

mongoose
  .connect(DB_HOST)
  .then(() => app.listen(PORT))
  .catch((error) => {
    console.log(error.message);
  });
